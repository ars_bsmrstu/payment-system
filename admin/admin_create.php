﻿<?php
	
	
		// Start the session
          session_start();
		  if(!$_SESSION["isLoginExamAdmin"])
{
	header("Location: index.php");
	die();
}
             include 'information.php';

          //echo password_hash("12345", PASSWORD_DEFAULT)."\n";
           mysql_connect($db_host,$db_username,$db_password);

            mysql_select_db($database);
           
$sql = "SELECT COUNT(*) AS `admin_row` FROM `admin`;";
			$result = mysql_query ($sql) or die (mysql_error ()); 
			while ($row = mysql_fetch_array($result)){
				$_SESSION['admin_row']=$row['admin_row'];	
			}
?>
<!doctype html>
<head>
<meta charset="utf-8">
<link rel="stylesheet" href="styles.css">
<link rel="stylesheet" href="style_admin.css">
<title>Admin</title>

<style type="text/css">

* { margin: 0; padding: 0; }

html { height: 100%; font-size: 62.5% }

body { height: 100%; background-color: #FFFFFF; font: 1.2em Verdana, Arial, Helvetica, sans-serif; }

<script type="text/javascript">
window.addEventListender('onresize', setTableWidth());

function setTableWidth() {
    document.getElementById("table").style.width = window.innerWidth + "px";
}

$(function() {
	$( "#Datepicker1" ).datepicker(); 
});
$(function() {
	$( "#Datepicker2" ).datepicker(); 
});
</script>


/* ==================== Form style sheet ==================== */

form { margin: 25px 0 0 29px; width: 370px; padding-bottom: 30px; }

fieldset { margin: 0 0 22px 0; border: 1px solid #095D92; padding: 12px 17px; background-color: #DFF3FF; }
legend { font-size: 1.1em; background-color: #095D92; color: #FFFFFF; font-weight: bold; padding: 4px 8px; }

label.float { float: left; display: block; width: 150px; margin: 4px 0 0 0; clear: left; }
label { display: block; width: auto; margin: 0 0 10px 0; }
label.spam-protection { display: inline; width: auto; margin: 0; }

input.inp-text, textarea, input.choose, input.answer { border: 1px solid #909090; padding: 3px; }
input.inp-text { width: 300px; margin: 0 0 8px 0; }
textarea { width: 400px; height: 150px; margin: 0 0 12px 0; display: block; }

input.choose { margin: 0 2px 0 0; }
input.answer { width: 40px; margin: 0 0 0 10px; }
input.submit-button { font: 1.4em Georgia, "Times New Roman", Times, serif; letter-spacing: 1px; display: block; margin: 23px 0 0 0; }

form br { display: none; }

/* ==================== Form style sheet END ==================== */

</style>
<link href="jQueryAssets/jquery.ui.core.min.css" rel="stylesheet" type="text/css" />
<link href="jQueryAssets/jquery.ui.theme.min.css" rel="stylesheet" type="text/css" />
<link href="jQueryAssets/jquery.ui.datepicker.min.css" rel="stylesheet" type="text/css" />
<script src="jQueryAssets/jquery-1.11.1.min.js" type="text/javascript"></script>
<script src="jQueryAssets/jquery.ui-1.10.4.datepicker.min.js" type="text/javascript"></script>
<script src="admin_script.js" type="text/javascript"></script>
</head>

<body>

<div class="main">
	<div class="logo">
		<img id="logo" src="bsmrstu.jpg">
	</div>
	<div id='cssmenu'>
       		             <ul>
                           <li><a href='admin.php'><span>Home</span></a></li>
                           <li><span><a href="admin_check.php">Check Status</a></span></li>
                           <li>&nbsp;</li>
                           <li class='active'><a href='admin_create.php'><span>Create Examination and Payment</span></a></li>
                           <li class='last'><a href='admin_modify.php'><span>Modify / Delete</span></a></li>
                           <li class='last'><a href='new_course.php'><span>ADD New Course</span></a></li>
                           <li class='last' style="float:right"><a href='admin_logout.php'><span>Logout</span></a></li>

                           </ul>
    </div>
    
    <div id="body" class="form">
    <?php
	if($_SESSION['admin_row'] > 0){
    echo '<script type="text/javascript">
  document.getElementById("body").innerHTML = "A payment method is already set. Delete the existing method to create new payment";
  </script>';
  die();
	}
  ?>
    
    <form action="create.php" method="post">
    <div id="course_registration" style="padding:15px">
  <label><input type="checkbox" name="course_registration" value="1">Course Registration</label>
</div>
		<!-- ============================== Fieldset 1 ============================== -->
		<fieldset>
		  <legend>বেতনাদির বিবরণ:</legend>
          
          <table id="table">
          <tr>
          <td>
          	 <p>
          	   <label for="input-one" class="float"><strong>হিসাব নং:</strong></label>
          	   <br />
          	   <input name="account_no" type="text" required="required" class="inp-text" id="input-one" size="30" /><br />
   	      
          
           <label for="input-one" class="float"><strong>সময়:</strong></label>
          </p>
          	 <p>&nbsp;   	      </p>
       	  <p>&nbsp;</p>
       	  <p>
            <input name="time_start" type="text" id="Datepicker3" required="required">
থেকে
<input name="time_end" type="text" id="Datepicker1" required="required">
          </p>
       	  <p>&nbsp;</p>
       	  <p>&nbsp;</p>
          	 <p>
          	   <label for="input-one" class="float"><strong>বেতন:</strong></label>
          	   <br />
          	   <input name="fees" type="number" value="0.0" class="inp-text" id="input-one" max="100000" min="0" size="30" /><br />
          	   
          	   <br />
          	   <label for="input-two" class="float"><strong>ভর্তি ফি:</strong></label>
          	   <br />
          	   <input name="admission_fee" type="number" value="0.0" class="inp-text"  id="input-two" max="100000" min="0" size="30" />
          	   
          	   <br /> 
          	   <label for="input-one" class="float"><strong>রেজিস্ট্রেশন ফি:</strong></label>
          	   <br />
          	   <input class="inp-text" name="registration_fee" value="0.0" max="100000" min="0" type="number"/><br />
          	   
          	   <br />
          	   <label for="input-two" class="float"><strong>পরিবহন:</strong></label>
          	   <br />
          	   <input class="inp-text" name="transport" value="0.0" max="100000" min="0" type="number"/><br />
          	   
          	   <br /> 
          	   <label for="input-one" class="float"><strong>লাইব্রেরী:</strong></label>
          	   <br />
          	   <input class="inp-text" name="library" value="0.0" max="100000" min="0" type="number"/><br />
          	   
          	   <br />
          	   <label for="input-two" class="float"><strong>কেন্দ্রীয় ক্রিড়া:</strong></label>
          	   <br />
          	   <input class="inp-text" name="central_sports" value="0.0" max="100000" min="0" type="number"/><br />
          	   
			    <br />
          	   <label for="input-two" class="float"><strong>কেন্দ্রীয়  কমনরুম:</strong></label>
          	   <br />
          	   <input class="inp-text" name="central_commonroom" value="0.0" max="100000" min="0" type="number"/><br />
			   
	           <br /> 
			    
          	   <label for="input-one" class="float"><strong>ছাত্র সংসদ   ( যদি থাকে)  :</strong></label>
          	   <br /> 
          	   <input class="inp-text" name="student_songsod" value="0.0" max="100000" min="0" type="number"/><br />
               
          	   <br />
			   
          	   <label for="input-one" class="float"><strong>ছাত্র কল্যাণ:</strong></label>
          	   <br />
          	   <input class="inp-text" name="student_welfare" value="0.0" max="100000" min="0" type="number"/>
          	   
               
               </td>
          	   <td>
               <label for="input-two" class="float"><strong>পরিচয় পত্র:</strong></label>
          	   <br />
          	   <input class="inp-text" name="id_card" value="0.0" max="100000" min="0" type="number"/><br/>
   
          	   <label for="input-one" class="float"><strong>চিকিৎসা ফি:</strong></label>
          	   <br />
          	   <input class="inp-text" name="medical_fee" value="0.0" max="100000" min="0" type="number"/><br />
          	   
          	   <br />
          	   <label for="input-two" class="float"><strong>সাংস্কৃতিক:</strong></label>
          	   <br />
          	   <input class="inp-text" name="cultural" value="0.0" max="100000" min="0" type="number"/><br />
          	   
          	   <br /> 
          	   <label for="input-one" class="float"><strong>রোভার্স স্কাউট:</strong></label>
          	   <br />
          	   <input class="inp-text" name="scout" value="0.0" max="100000" min="0" type="number"/><br />
          	   
          	   <br />
          	   <label for="input-two" class="float"><strong>বিএনসিসি:</strong></label>
          	   <br />
          	   <input class="inp-text" name="bncc" value="0.0" max="100000" min="0" type="number"/><br />
          	   
          	   <br /> 
          	   <label for="input-one" class="float"><strong>সিলেবাস:</strong></label>
          	   <br />
          	   <input class="inp-text" name="syllabus" value="0.0" max="100000" min="0" type="number"/><br />
          	   
          	   <br />
          	   <label for="input-two" class="float"><strong>শিক্ষাপঞ্জী(ডায়েরী):</strong></label>
          	   <br />
          	   <input class="inp-text" name="diary" value="0.0" max="100000" min="0" type="number"/><br />
          	   
          	   <br /> 
          	   <label for="input-one" class="float"><strong>কম্পিউটার:</strong></label>
          	   <br />
          	   <input class="inp-text" name="computer" value="0.0" max="100000" min="0" type="number"/><br />
          	   
          	   <br />
          	   <label for="input-two" class="float"><strong>বিভাগীয় ফি:</strong></label>
          	   <br />
          	   <input class="inp-text" name="departmental_fee" value="0.0" max="100000" min="0" type="number"/><br />
          	   
          	   <br /> 
          	   <label for="input-one" class="float"><strong>সেমিস্টার পরীক্ষার ফি:</strong></label>
          	   <br />
          	   <input class="inp-text" name="semester_exam_fee" value="0.0" max="100000" min="0" type="number"/><br />
               
          		        	   
          	   <br />
          	   <label for="input-two" class="float"><strong>ব্যাবহারিক পরীক্ষার ফি:</strong></label>
          	   <br />
          	   <input class="inp-text" name="practical_exam_fee" value="0.0" max="100000" min="0" type="number"/>
          	   
               </td>
  				<td>
                <label for="input-one" class="float"><strong>কেন্দ্র ফি:</strong></label>
          	   <br />
          	   <input class="inp-text" name="center_fee" value="0.0" max="100000" min="0" type="number"/><br/>
          	   
          
          	   <label for="input-two" class="float"><strong>ট্রান্সক্রিপ্ট ফি:</strong></label>
          	   <br />
          	   <input class="inp-text" name="transcript_fee" value="0.0" max="100000" min="0" type="number"/><br />
          	   
          	   <br /> 
          	   <label for="input-one" class="float"><strong>মুল সার্টিফিকেট:</strong></label>
          	   <br />
          	   <input class="inp-text" name="main_certificate" value="0.0" max="100000" min="0" type="number"/><br />
          	   
          	   <br />
          	   <label for="input-two" class="float"><strong>প্রভিশনাল সার্টিফিকেট:</strong></label>
          	   <br />
          	   <input class="inp-text" name="provitional_certificate" value="0.0" max="100000" min="0" type="number"/><br />
          	   
          	   <br />
          	   <label for="input-one" class="float"><strong>জামানত:</strong></label>
          	   <br />
          	   <input class="inp-text" name="jamanot" value="0.0" max="100000" min="0" type="number"/><br />
          	   
          	   <br />
          	   <label for="input-two" class="float"><strong>ভেরিফিকেশন/এটাসটেশন ফি:</strong></label>
          	   <br />
          	   <input class="inp-text" name="verification_fee" value="0.0" max="100000" min="0" type="number"/><br />
          	   
          	   <br />
          	   <label for="input-one" class="float"><strong>প্রবেশ পত্র:</strong></label>
          	   <br />
          	   <input class="inp-text" name="admit_card" value="0.0" max="100000" min="0" type="number"/><br />
          	   
          	   <br />
          	   <label for="input-two" class="float"><strong>বেতন বই ফি:</strong></label>
          	   <br />
          	   <input class="inp-text" name="baton_boi_fee" value="0.0" max="100000" min="0" type="number"/><br />
          	   
          	   <br />  
          	   <label for="input-one" class="float"><strong>জরিমানা:</strong></label>
          	   <br />
          	   <input class="inp-text" name="jorimana" value="0.0" max="100000" min="0" type="number"/><br />
          	   
          	   <br />
          	   <label for="input-two" class="float"><strong>প্রকাশনা:</strong></label>
          	   <br />
          	   <input class="inp-text" name="publication" value="0.0" max="100000" min="0" type="number"/><br />
          	   
          	   <br /> 
          	   <label for="input-two" class="float"><strong>অন্যান্য:</strong></label>
          	   <br />
          	   <input class="inp-text" name="others" value="0.0" max="100000" min="0.0" type="number"/><br />
   	      </p>
          </td>
          </tr>
   			</table>
                            
		</fieldset>
		<!-- ============================== Fieldset 1 end ============================== -->


		<!-- ============================== Fieldset 2 ============================== -->

		<input style="margin:auto;display:block;" class="btn" type="submit" alt="SUBMIT" name="Submit" value="SUBMIT" />
	</form>    
    </div>
<div>
<script type="text/javascript">
$(function() {
	$( "#Datepicker3" ).datepicker({
		dateFormat:"yy-mm-dd"
	}); 
});
$(function() {
	$( "#Datepicker1" ).datepicker({
		dateFormat:"yy-mm-dd"
	}); 
});
</script>
</body>

</html>
