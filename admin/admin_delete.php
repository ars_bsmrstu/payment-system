<?php
// Start the session
          session_start();
		  if(!$_SESSION["isLoginExamAdmin"])
{
	header("Location: index.php");
	die();
}
             include 'information.php';

// Create connection
$conn = new mysqli($db_host, $db_username, $db_password, $database);
// Check connection
if ($conn->connect_error) {
    header("Location: error.html");
	die();
} 

// sql to delete a record
$sql = "DELETE FROM `admin` WHERE 1";

if ($conn->query($sql) === TRUE) {
    header("Location: success.html");
	die();
} else {
    header("Location: error.html");
	die();
}

$conn->close();
?>