<?php
	
	
		// Start the session
          session_start();
		  if(!$_SESSION['isLoginExamAdmin'])
{
	header("Location: index.php");
	die();
}
?>
<!doctype html>
<head>
<meta charset="utf-8">
<link rel="stylesheet" href="styles.css">
<link rel="stylesheet" href="style_admin.css">
<title>Admin</title>

<style type="text/css">

* { margin: 0; padding: 0; }

html { height: 100%; font-size: 62.5% }

body { height: 100%; background-color: #FFFFFF; font: 1.2em Verdana, Arial, Helvetica, sans-serif; }


/* ==================== Form style sheet ==================== */

form { margin: 25px 0 0 29px; width: 370px; padding-bottom: 30px; }

fieldset { margin: 0 0 22px 0; border: 1px solid #095D92; padding: 12px 17px; background-color: #DFF3FF; }
legend { font-size: 1.1em; background-color: #095D92; color: #FFFFFF; font-weight: bold; padding: 4px 8px; }

label.float { float: left; display: block; width: 250px; margin: 4px 0 0 0; clear: left; }
label { display: block; width: auto; margin: 0 0 10px 0; }
label.spam-protection { display: inline; width: auto; margin: 0; }

input.inp-text, textarea, input.choose, input.answer { border: 1px solid #909090; padding: 3px; }
input.inp-text { width: 300px; margin: 0 0 8px 0; }
textarea { width: 400px; height: 150px; margin: 0 0 12px 0; display: block; }

input.choose { margin: 0 2px 0 0; }
input.answer { width: 40px; margin: 0 0 0 10px; }
input.submit-button { font: 1.4em Georgia, "Times New Roman", Times, serif; letter-spacing: 1px; display: block; margin: 23px 0 0 0; }

form br { display: none; }

/* ==================== Form style sheet END ==================== */

</style>

<script>
function myFunction() {
    alert("Your Data is Inserted");
}
</script>

<link href="jQueryAssets/jquery.ui.core.min.css" rel="stylesheet" type="text/css" />
<link href="jQueryAssets/jquery.ui.theme.min.css" rel="stylesheet" type="text/css" />
<link href="jQueryAssets/jquery.ui.datepicker.min.css" rel="stylesheet" type="text/css" />
<script src="jQueryAssets/jquery-1.11.1.min.js" type="text/javascript"></script>
<script src="jQueryAssets/jquery.ui-1.10.4.datepicker.min.js" type="text/javascript"></script>
</head>

<body>

<div class="main">
	<div class="logo">
		<img id="logo" src="bsmrstu.jpg">
	<div>
	<div id='cssmenu'>
       		             <ul>
                           <li><a href='admin.php'><span>Home</span></a></li>
                           <li><span><a href="admin_check.php">Check Status</a></span></li>
                           <li>&nbsp;</li>
                           <li class='last'><a href='admin_create.php'><span>Create Examination and Payment</span></a></li>
                           <li class='last'><a href='admin_modify.php'><span>Modify / Delete</span></a></li>
                           <li class='active'><a href='new_course.php'><span>ADD New Course</span></a></li>
                           <li class='last' style="float:right"><a href='admin_logout.php'><span>Logout</span></a></li>
                           </ul>
    </div>
    
    <div class="form">
    
    <form action="course.php" method="post" style="height: 500px">
		<!-- ============================== Fieldset 1 ============================== -->
		<fieldset style="height: 415px">
			<legend>ADD NEW COURSE:</legend>
				<label for="input-one" class="float"><strong>Course Code:</strong></label><br />
				<input class="inp-text" name="course_code" id="input-one" type="text" size="30" /><br />

				<label for="input-two" class="float"><strong>Course Title:</strong></label><br />
				<br/><input class="inp-text" name="course_title"  id="input-two" type="text" size="30" />
				
				<label for="input-two" class="float"><strong>Credits:</strong></label><br />
				<br/><input class="inp-text" name="credits"  id="input-two" type="number" min="0" size="30" />
				
				
				<label for="input-two" class="float"><strong>Contact_hours:</strong></label><br />
				<br/><input class="inp-text" name="contact_hours"  id="input-two" type="number" min="0" size="30" />
				
				<label for="input-two" class="float"><strong>Full naem of Dpartment:</strong></label><br />      
				<br/><input class="inp-text" name="departments"  id="input-two" type="text" size="30" />
				
				<label for="input-two" class="float" style="width: 294px"><strong>Year:</strong></label><br />

				<br/><br/><select name="year" class="inp-text"  id="input-two">
				<option value="First Year">First Year</option>
				<option value="Second Year">Second Year</option>
				<option value="Thired Year">Thired Year</option>
				<option value="Fourth Year">Fourth Year</option>
				</select><br/>
				
				
				<br />
				
				
				<br/><label for="input-two" class="float" style="width: 292px"><strong>Semester:</strong>
				<select name="semister">
				<option value="First Semister">First Semister</option>
				<option value="Second Semister">Second Semister</option>
				</select></label>

				<label for="input-two" class="float"><strong>Session(20XX-20XX):</strong></label><br />
				<br/>&nbsp;<input class="inp-text" name="session"  id="input-two" type="text" size="30"  /></fieldset>
		<!-- ============================== Fieldset 1 end ============================== -->


		

		
		<p><input class="submit-button" type="submit" alt="SUBMIT" name="Submit" value="SUBMIT" onclick="myFunction()" /></p>
	</form>
    </div>
<div>
</body>

</html>
