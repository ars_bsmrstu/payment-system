<?php
// Start the session
session_start();
if(!$_SESSION["isLoginExamAdmin"])
{
	header("Location: index.php");
	die();
}

	$_SESSION['isLoginExamAdmin'] = false;
	
	header("Location: index.php");
	die();
?>