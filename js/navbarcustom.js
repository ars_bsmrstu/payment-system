// JavaScript Document
$(".navbar-nav li a").click(function (event) {
    // check if window is small enough so dropdown is created
    var toggle = $(".navbar-toggle").is(":visible");
    if (toggle) {
      $(".navbar-collapse").collapse('hide');
    }
  });
  // make nav button active and deactive
  $('ul li').click( function() {
    $(this).addClass('active').siblings().removeClass('active');
  });