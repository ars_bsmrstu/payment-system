<?php
// Start the session
session_start();
if(!$_SESSION["isLogin"])
{
	header("Location: index.php");
	die();
}

	$_SESSION['isLogin'] = false;
	
	header("Location: index.php");
	die();
?>