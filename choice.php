<?php
session_start();
$q = intval($_GET['q']);
include 'information.php';

$counter=1;

// Create connection
$conn = new mysqli($db_host, $db_username, $db_password, $database);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$sql = 'SELECT * FROM `course` WHERE `year`='.$q.' AND `session`=(SELECT `session_code` FROM `session` WHERE `session_name`="'.$_SESSION['admission_session'].'") AND  `department`='.$_SESSION['department'];
$result = $conn->query($sql);

if ($result->num_rows > 0) {
    // output data of each row
    while($row = $result->fetch_assoc()) {
        echo '<p id="'.$row['credits'].'" title="'.$row['course_code'].'" onClick="addCourse(this.innerHTML, this.id, this.title)" class="bottom" >'.$row['course_code'] .' : '.$row['course_title'] . '</p>';
		$counter++;
    }
} else {
    echo "0 results";
}
$conn->close();
?>