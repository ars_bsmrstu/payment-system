<?php
// Start the session
session_start();

if(!$_SESSION["isLoginDepartment"])
{
	header("Location: ../index.php");
	die();
}

		include '../../information.php';




		mysql_connect($db_host,$db_username,$db_password);

        mysql_select_db($student_database);
		
		$admission_roll_no=$_POST['admission_roll_no'];
		$pin=$_POST['admission_roll_no'];
		
		$hsc_roll_no=$_POST['hsc_roll_no'];
		$sql = "SELECT * FROM `registration_all__2` WHERE PIN='$admission_roll_no';";
            $result = mysql_query ($sql) or die (mysql_error ()); 
			while ($row = mysql_fetch_array($result)){
				
				$student_name=$row["SSC NAME"];
				$fathers_name=$row["SSC F_NAME"];
				$mothers_name=$row["SSC M_NAME"];
				$phone_number=$row["MOB. NUMBER"];
				$ssc_passing_year=$row["SSC PASS_YEAR"];
				$ssc_board=$row["SSC BOARD_NAME"];
				$ssc_roll=$row["SSC ROLL_NO"];
				$ssc_gpa=$row["SSC WITH GPA"];
				$hsc_passing_year=$row["HSC PASS_YEAR"];
				$hsc_board=$row["HSC BOARD_NAME"];
				$hsc_roll=$row["HSC ROLL_NO"];
				$hsc_gpa=$row["HSC With GPA"];
				
			
			}
			
			
			if(($hsc_roll_no == NULL)||($hsc_roll_no != $hsc_roll))
			 {
				 header("Location: mismatch.html");
				 die();
			 }
?>

<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Applicant Info</title>
<link href="style2.css" rel="stylesheet" type="text/css">
<link href="jQueryAssets/jquery.ui.core.min.css" rel="stylesheet" type="text/css">
<link href="jQueryAssets/jquery.ui.theme.min.css" rel="stylesheet" type="text/css">
<link href="jQueryAssets/jquery.ui.datepicker.min.css" rel="stylesheet" type="text/css">
<script src="jQueryAssets/jquery-1.11.1.min.js" type="text/javascript"></script>
<script src="jQueryAssets/jquery.ui-1.10.4.datepicker.min.js" type="text/javascript"></script>
<script src="js_home.js" type="text/javascript"></script>
</head>

<body>
<div class="body">
  <div id="banner"> <img src="bsmrstu.jpg" width="100%" height="50%" alt=""/> </div>
  <div class="form">
    <form class="form-style-4" action="image_insert.php" method="post">
      <h2 id="form_name" style="color:#2D7136; text-align:center">Registration Form:</h2>
      <div class="first">
        <div class="form_main">
          <input type="hidden" name="pin" value="<?php echo $pin; ?>" />
          <label for="admission_roll_no"> <span>ID No :</span>
            <input name="admission_roll_no" onFocus="instruction(admission_roll_no)" type="text" required="true" class="session" id="admission_roll_no"  maxlength="11" value="2015"/>
          </label>
          <label for="admission_session"> <span>Admission Session :</span>
            <input name="admission_session" type="text" required="true" class="session"  id="admission_session" pattern="20[0-9]{2}\-[0-9]{2}" onFocus="instruction(admission_session)" value="2015-16" maxlength="10" readonly style="color:#9A9A9A" />
          </label>
          <label for="department"> <span>Departemnts :</span>
            <select name="department" id="department" onFocus="instruction(department)">
              <option value="Analytical and Environmental Chemistry">Analytical and Environmental Chemistry</option>
              <option value="Agriculture">Agriculture</option>
              <option value="Accounting and Information Systems">Accounting and Information Systems</option>
              <option value="Applied Physics, Electronics and Communication Engineering">Applied Physics, Electronics and Communication Engineering</option>
              <option value="Biochemistry and Molecular Biology">Biochemistry and Molecular Biology</option>
              <option value="Biotechnology and Genetic Engineering">Biotechnology and Genetic Engineering</option>
              <option value="Bangla">Bangla</option>
              <option value="Computer Science and Engineering">Computer Science and Engineering</option>
              <option value="Economics">Economics</option>
              <option value="Electrical and Electronic Engineering">Electrical and Electronic Engineering</option>
              <option value="English">English</option>
              <option value="International Relation">International Relation</option>
              <option value="Law">Law</option>
              <option value="Management Studies">Management Studies</option>
              <option value="Marketing">Marketing</option>
              <option value="Mathematics">Mathematics</option>
              <option value="Pharmacy">Pharmacy</option>
              <option value="Public Administration">Public Administration</option>
              <option value="Sociology">Sociology</option>
              <option value="Statistics">Statistics</option>
            </select>
          </label>
          <br>
          <label>Student Name: </label>
          <label for="student_name"><span>In English:</span>
            <input style="color:#9A9A9A" name="student_name"  type="text"  required="true" class="session" id="student_Name" value="<?php echo $student_name; ?>" readonly onFocus="instruction(student_name)" />
          </label>
          <label for="student_name_bng"><span>বাংলায় :</span>
            <input  name="student_name_bng"  type="text"  required="true" class="session" id="student_Name" onFocus="instruction(student_name)" />
          </label>
          <label>Father's Name: </label>
          <label for="fathers_name"> <span>In English:</span>
            <input style="color:#9A9A9A" name="fathers_name" value="<?php echo $fathers_name; ?>" readonly type="text" required class="form_table" id="fathers_name" onFocus="instruction(fathers_name)" >
          </label>
          <label for="father_name_bng"><span>বাংলায় :</span>
            <input  name="father_name_bng"  type="text"  required="true" class="session" id="father_Name" onFocus="instruction(student_name)" />
          </label>
          <label>Mother's Name : </label>
          <label for="mothers_name"> <span>In English :</span>
            <input style="color:#9A9A9A" name="mothers_name" type="text" required id="mothers_name" value="<?php echo $mothers_name; ?>" readonly onFocus="instruction(mothers_name)" />
          </label>
          <label for="mother_name_bng"><span>বাংলায় :</span>
            <input  name="mother_name_bng"  type="text"  required="true" class="session" id="mother_Name" onFocus="instruction(student_name)" />
          </label>
          <label for="nationality"> <span>Nationility :</span>
            <input name="nationality" onFocus="instruction(nationality)" type="text" value="Bangladeshi" readonly required id="nationality" pattern="^[A-Za-z\s]{1,}" maxlength="20" />
          </label>
          <label for="religion"> <span>Religion :</span>
            <select name="religion" id="religion" onFocus="instruction(religion)">
              <option value="Islam">Islam</option>
              <option value="Hinduism">Hinduism</option>
              <option value="Christianity">Christianity</option>
              <option value="Buddhism">Buddhism</option>
              <option value="Others">Others</option>
            </select>
          </label>
          <label for="date_of_birth"> <span>Date of Birth :</span>
            <input name="date_of_birth" onFocus="instruction(date_of_birth)" type="text" required id="date_of_birth" pattern="^(19|20)\d\d\-(0?[1-9]|1[012])\-(0?[1-9]|[12][0-9]|3[01])$" maxlength="10"/>
          </label>
          <label for="phone_number"> <span>Phone Number :</span>
            <input name="phone_number" onFocus="instruction(phone_number)" type="text" required id="phone_number" pattern="\+{0,1}[0-9]{4,18}" maxlength="16" value="+880<?php echo $phone_number; ?>" />
          </label>
          <label for="email"> <span>Email Address :</span>
            <input name="email" onFocus="instruction(email)" type="email" id="email" maxlength="40"/>
          </label>
          <label for="blood_group"> <span>Blood Group :</span>
            <select name="blood_group" id="blood_group" onFocus="instruction(blood_group)">
              <option value="unknown">Unknown</option>
              <option value="A+">A+</option>
              <option value="A-">A-</option>
              <option value="B+">B+</option>
              <option value="B-">B-</option>
              <option value="O+">O+</option>
              <option value="O-">O-</option>
              <option value="AB+">AB+</option>
              <option value="AB-">AB-</option>
            </select>
          </label>
        </div>
        <div > <img id="image" src="pictures/<?php echo $pin ?>.jpg"> </div>
        <div>
          <label id="instruction_first" style="margin-top:9em; float:right; margin-right:20%; color:#FF0004;"></label>
        </div>
      </div>
      <div class="form_table">
        <table width="100%" border="1">
          <caption>
          <label id="instruction_table" style="color:#FF0004;"></label>
          <strong>Previous Exam information:</strong>
          </caption>
          <tbody>
            <tr>
              <th style="width:15%" >ExamName</th>
              <th style="width:11%" >Passing Year</th>
              <th style="width:30%">Institution Name </th>
              <th style="width:16%" >Bord/University</th>
              <th style="width:14%">Exam Roll Number</th>
              <th style="width:8%" >Division/GPA/CGPA</th>
            </tr>
            <tr>
              <th scope="row">SSC/Equivalent</th>
              <td><input style="text-align:center; color:#9A9A9A; width:98%" name="ssc_passing_year"  type="number" required id="ssc_passing_year"  max="2099" min="1950" step="1" value="<?php echo $ssc_passing_year; ?>" readonly onFocus="instruction(ssc_passing_year)"></td>
              <td><input name="ssc_institute" onFocus="instruction(ssc_institute)"  required id="ssc_institute" style="width:98%" maxlength="50"></td>
              <td style="text-align:center; color:#9A9A9A"><?php echo $ssc_board; ?></td>
              <input type="hidden" name="ssc_board" value= "<?php echo $ssc_board; ?>">
              <td><input style="text-align:center; color:#9A9A9A; width:98%" name="ssc_roll"  required id="ssc_roll" value="<?php echo $ssc_roll;  ?>" readonly onFocus="instruction(ssc_roll)"></td>
              <td><input style="text-align:center; color:#9A9A9A; width:98%" name="ssc_gpa"  type="number" required id="ssc_gpa"  max="5" min="0" step=".01" value="<?php echo $ssc_gpa; ?>" readonly onFocus="instruction(ssc_gpa)"></td>
            </tr>
            <tr>
              <th scope="row">HSC/Equivalent</th>
              <td><input style="text-align:center; color:#9A9A9A; width:98%" name="hsc_passing_year"  type="number" required id="hsc_passing_year"  max="2099" min="1950" step="1" value="<?php echo $hsc_passing_year; ?>" readonly onFocus="instruction(hsc_passing_year)"></td>
              <td><input name="hsc_institute" onFocus="instruction(hsc_institute)" required id="hsc_institute" style="width:98%" maxlength="50"></td>
              <td style="text-align:center; color:#9A9A9A"><?php echo $hsc_board; ?></td>
              <input type="hidden" name="hsc_board" value= "<?php echo $hsc_board; ?>">
              <td><input style="text-align:center; color:#9A9A9A; width:98%" name="hsc_roll"  required id="hsc_roll"  value="<?php echo $hsc_roll; ?>" readonly onFocus="instruction(hsc_roll)"></td>
              <td><input style="text-align:center; color:#9A9A9A; width:98%" name="hsc_gpa"  type="number" required id="hsc_gpa" max="5" min="0" step=".01" value="<?php echo $hsc_gpa; ?>" readonly onFocus="instruction(hsc_gpa)"></td>
            </tr>
            <tr>
              <th scope="row">&nbsp;</th>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <th scope="row">&nbsp;</th>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
            </tr>
          </tbody>
        </table>
      </div>
      <div class="address">
        <label id="instruction_last"><br>
        </label>
        <label for="present_address"> <span>Present Address :</span>
          <input name="present_address" onFocus="instruction(present_address)" type="text" required="true" id="present_address" style="width:50%" maxlength="140" />
        </label>
        <h4>Permanent Address :</h4>
        <label for="village"> <span> Village :</span>
          <input name="village" onFocus="instruction()" type="text" required="true" id="village" maxlength="30" />
        </label>
        <label for="post"> <span> Post :</span>
          <input name="post" onFocus="instruction()" type="text" required="true" id="post" maxlength="30" />
        </label>
        <label for="upazilla"> <span>Upazilla :</span>
          <input name="upazilla" onFocus="instruction()" type="text" required="true" id="upazilla" maxlength="30" />
        </label>
        <label for="zilla"> <span>Zilla :</span>
          <select name="zilla" id="zilla" onFocus="instruction()">
            <option value="Bagerhat">Bagerhat</option>
            <option value="Bandarban">Bandarban</option>
            <option value="Barguna">Barguna</option>
            <option value="Barisal">Barisal</option>
            <option value="Bhola">Bhola</option>
            <option value="Bogra">Bogra</option>
            <option value="Brahmanbaria">Brahmanbaria</option>
            <option value="Chandpur">Chandpur</option>
            <option value="Chapainawabganj">Chapainawabganj</option>
            <option value="Chittagong">Chittagong</option>
            <option value="Chuadanga">Chuadanga</option>
            <option value="Comilla">Comilla</option>
            <option value="Coxs Bazar">Cox's Bazar</option>
            <option value="Dhaka">Dhaka</option>
            <option value="Dinajpur">Dinajpur</option>
            <option value="Faridpur">Faridpur</option>
            <option value="Feni">Feni</option>
            <option value="Gaibandha">Gaibandha</option>
            <option value="Gazipur">Gazipur</option>
            <option value="Gopalganj">Gopalganj</option>
            <option value="Habiganj">Habiganj</option>
            <option value="Jamalpur">Jamalpur</option>
            <option value="Jessore">Jessore</option>
            <option value="Jhalokati">Jhalokati</option>
            <option value="Jhenaidah">Jhenaidah</option>
            <option value="Joypurhat">Joypurhat</option>
            <option value="Khagrachhari">Khagrachhari</option>
            <option value="Khulna">Khulna</option>
            <option value="Kishoreganj">Kishoreganj</option>
            <option value="Kurigram">Kurigram</option>
            <option value="Kushtia">Kushtia</option>
            <option value="Lakshmipur">Lakshmipur</option>
            <option value="Lalmonirhat">Lalmonirhat</option>
            <option value="Madaripur">Madaripur</option>
            <option value="Magura">Magura</option>
            <option value="Manikganj">Manikganj</option>
            <option value="Meherpur">Meherpur</option>
            <option value="Moulvibazar">Moulvibazar</option>
            <option value="Munshiganj">Munshiganj</option>
            <option value="Mymensingh">Mymensingh</option>
            <option value="Naogaon">Naogaon</option>
            <option value="Narail">Narail</option>
            <option value="Narayanganj">Narayanganj</option>
            <option value="Narsingdi">Narsingdi</option>
            <option value="Natore">Natore</option>
            <option value="Netrakona">Netrakona</option>
            <option value="Nilphamari">Nilphamari</option>
            <option value="Noakhali">Noakhali</option>
            <option value="Pabna">Pabna</option>
            <option value="Panchagarh">Panchagarh</option>
            <option value="Patuakhali">Patuakhali</option>
            <option value="Pirojpur">Pirojpur</option>
            <option value="Rajbari">Rajbari</option>
            <option value="Rajshahi">Rajshahi</option>
            <option value="Rangamati">Rangamati</option>
            <option value="Rangpur">Rangpur</option>
            <option value="Satkhira">Satkhira</option>
            <option value="Shariatpur">Shariatpur</option>
            <option value="Sherpur">Sherpur</option>
            <option value="Sirajganj">Sirajganj</option>
            <option value="Sunamganj">Sunamganj</option>
            <option value="Sylhet">Sylhet</option>
            <option value="Tangail">Tangail</option>
            <option value="Thakurgaon">Thakurgaon</option>
          </select>
        </label>
      </div>
      <div id="submit_buttom" style="padding-left:33%">
        <label> <span>&nbsp;</span> <input type="submit" value="Submit" <a href="imageUpload.html" class="myButton"> </label>
      </div>
    </form>
  </div>
</div>
</body>
</html>
