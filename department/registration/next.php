<?php
// Start the session
session_start();

if(!$_SESSION["isLoginDepartment"])
{
	header("Location: index.php");
	die();
}
?>

<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Registration &amp; Payment</title>
<link rel="stylesheet" href="normalize.css">
	<link rel="stylesheet" href="style.css">
</head>

<body>
<div id="banner"> <img src="bsmrstu.jpg" width="100%" height="auto" alt=""/> </div>

<section class="loginform cf">
		<form name="login" action="home.php" method="post" accept-charset="utf-8">
			<ul>
				<li>
					<label for="usermail">Admission PIN</label>
					<input type="number" name="admission_roll_no" placeholder="Admission PIN" required>
				</li>
                
                <li>
					<label for="hsc_roll_no">HSC Roll No</label>
					<input type="number" name="hsc_roll_no" placeholder="HSC Roll" required>
				</li>
				
					<input type="submit" value="Next">
			</ul>
		</form>
	</section>
  <div class="parent">
 <a href="../logout.php" class="logoutButton">logout</a>
 <a href="../index.php" class="home_button">Home</a>
 </div>

</body>
</html>
