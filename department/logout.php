<?php
// Start the session
session_start();
if(!$_SESSION["isLoginDepartment"])
{
	header("Location: index.php");
	die();
}

	$_SESSION['isLoginDepartment'] = false;
	
	header("Location: index.php");
	die();
?>