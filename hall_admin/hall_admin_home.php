<?php
session_start();

if(!$_SESSION["isLoginHallAdmin"])
{
	header("Location: index.php");
	die();
}

error_reporting(0);
include 'information.php';

mysql_connect("$db_host","$db_username","$db_password");

        mysql_select_db("$database");
		
		$username=$_SESSION['username'];

        $sql = "SELECT * FROM `hall_admin` WHERE username='$username';";
        $result = mysql_query ($sql) or die (mysql_error ());
       while ($row = mysql_fetch_array($result)){     // here too, you put a space between it
             
			$hall=$row['hall'];
		}
		
		
?>

<!doctype html>
<html lang="en-US">
<head>
  <meta charset="utf-8">
  <meta http-equiv="Content-Type" content="text/html">
  <title>Hall Admin</title>
  <meta name="author" content="Jake Rocheleau">
  <link rel="shortcut icon" href="Bsmrstu_logo.jpg">
   <link rel="icon" href="Bsmrstu_logo.jpg">
  <link rel="stylesheet" type="text/css" media="all" href="css/styles.css">
  <link rel="stylesheet" type="text/css" media="all" href="css/styles_hall_admin_home.css">
  <link href="jQueryAssets/jquery.ui.core.min.css" rel="stylesheet" type="text/css">
  <link href="jQueryAssets/jquery.ui.theme.min.css" rel="stylesheet" type="text/css">
  <link href="jQueryAssets/jquery.ui.datepicker.min.css" rel="stylesheet" type="text/css">
  <script type="text/javascript" src="js/jquery-1.11.1.min.js"></script>
  <script src="jQueryAssets/jquery.ui-1.10.4.datepicker.min.js" type="text/javascript"></script>
</head>

<body>
  <header id="hero">
                <img src="images/bsmrstu.jpg" alt=""/> </header>
  <!-- http://unsplash.com/post/96058742904/download-by-jonas-nilsson-lee -->
  
  <div id="content">
    <div class="wrapper">
      <h1><?php $sql = "SELECT * FROM `hall` WHERE hall_id='$hall';";
            $result = mysql_query ($sql) or die (mysql_error ()); 
			while ($row = mysql_fetch_array($result)){
				
				$hall=$row['hall_name'];

			}
			$_SESSION['hall'] = $hall;
		echo $hall;  ?></h1>
      
      <div id="jobs" class="clearfix">
        <div id="jobs-list">
          <ul>
            <li><a href="#job1" class="active">Home</a></li>
            <li><a href="#job2">New Student</a></li>
            <li><a href="#job3">Update/Delete Info</a></li>
            <li><a href="#job4">Account</a></li>
            <li><a href="#job5">Information</a></li>
            <li><a href="#job6">LOgout</a></li>
            
          </ul>
        </div>
        
        <div id="job-info">
          <div id="job1" class="jobitem displayed">
            <h3>Home</h3>
            
            
          
            
          </div><!-- @end #job1 -->

          
          <!-- @start #job2 -->
          <div id="job2" class="jobitem">
            
             <form class="form-style-4" action="student_info.php" method="post">
             		<h3 style="color:#EDE6E6">Add New Student</h3>
             	 <label for="admission_roll_no"> <span>ID No :</span>
                  <input name="admission_roll_no"  type="text"   required="true"  id="admission_roll_no"   maxlength="11" />
                </label>
                <div id="submit_buttom" >
        <label> <input type="submit" value="Next"  class="myButton"> </label>
        </div>
        
             </form>
             
          
            
          </div><!-- @end #job2 -->
          
          
          <!-- @start #job3 -->
          <div id="job3" class="jobitem">
            
          
			   <form class="form-style-4" action="update_room.php" method="post">
			  <p style="color:white"><strong>Update/Delete Information</strong></p>
              
			  <label for="admission_roll_no"> <span>Enter ID No :</span>
                  <input name="admission_roll_no"  type="text"   required="true"  id="admission_roll_no"   maxlength="11" />
                </label>
          
          <div id="submit_buttom" >
            
        <label> <input type="submit" value="Submit"  class="myButton"> </label>
        </div>
        </form>
          
            
          </div><!-- @end #job3 -->

          
          <!-- @start  Account #job4 -->
          <div id="job4" class="jobitem">
            
           
            <a href="hall_account.php" class="applybtn"><span>New Account</span></a>
             <a href="update_hall_account.php" class="applybtn"><span>Update Account</span></a>
             <a href="delete.php" class="applybtn"><span>Delete</span></a>
             
           
          
            
          </div><!-- @end #job4 -->
          
          <!-- @start #job5 -->
          <div id="job5" class="jobitem">
            <h3>D</h3>
          	
          
          </div><!-- @end #job5 -->
          
          <!-- @start #job6 -->
          <div id="job6" class="jobitem">
            <h3>Logout</h3>
          	
           <a href="logout.php" class="applybtn"><span>Yes</span></a>
          <a href="hall_admin_home.php" class="applybtn"><span>No</span></a>
          
          </div>
          <!-- @end #job6 -->
          
        </div>
      </div>
    </div>
  </div>
<script type="text/javascript">
$(function(){
  $jobslist = $('#jobs-list ul li a');
  
  $($jobslist).on('click', function(e){
    e.preventDefault();
    var newcontent = $(this).attr('href');

    if(!$(this).hasClass('active')) {
      $('#jobs-list ul li a.active').removeClass('active');
    }
    $(this).addClass('active');
    
    if(!$(newcontent).hasClass('displayed')) {
      $('.jobitem.displayed').removeClass('displayed');
      $(newcontent).addClass('displayed');
    }
  });
});
$(function() {
	$( "#Datepicker1" ).datepicker(); 
});
</script>
</body>
</html>