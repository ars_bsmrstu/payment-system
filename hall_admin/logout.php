<?php
// Start the session
session_start();
if(!$_SESSION["isLoginHallAdmin"])
{
	header("Location: index.php");
	die();
}

	$_SESSION['isLoginHallAdmin'] = false;
	
	header("Location: index.php");
	die();
?>