function show(str) {
    if (str == "") {
        document.getElementById("choice").innerHTML = "";
        return;
    } else { 
        if (window.XMLHttpRequest) {
            // code for IE7+, Firefox, Chrome, Opera, Safari
            xmlhttp = new XMLHttpRequest();
        } else {
            // code for IE6, IE5
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function() {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                document.getElementById("choice").innerHTML = xmlhttp.responseText;
            }
        };
        xmlhttp.open("GET","choice.php?q="+str,true);
        xmlhttp.send();
    }
	warning(0);
}

var credit=0;
var per_cr_fee=100;
var credit_limit=8;
var total=0;

function addCourse(str, str_cr, course_code) {
    if (str == "") 
        return;
     else {
		 var cr = parseFloat(str_cr);
		var x = document.getElementsByClassName("top");
		for(var i=0; i<x.length; i++)
		{
			if(x[i].innerHTML == str)
			{
				var alreadySelected=true;
			}
		}
		
		if(!alreadySelected){
		if(addCredit(cr))
		{
		document.getElementById("selected").innerHTML=document.getElementById("selected").innerHTML+'<p class="top" id="'+cr+'" title="'+course_code+'" onclick="removeCourse(this.innerHTML, this.id)">'+str+'</p>';
		total_fee();
		removeCourseBottom(str);
		warning(0);		
		}
		else
		warning(2);
		}
		else {
		warning(1);
		}
		
	}		
}
		
function removeCourse(str, cr) {
    if (str == "") {
        return;
    } else {
		var x = document.getElementsByClassName("top");
		for(var i=0; i<x.length; i++)
		{
			if(x[i].innerHTML == str)
			{
				var parent = document.getElementById("selected");
				parent.removeChild(x[i]);
				removeCredit(cr);
				total_fee();
				break;
			}
		}
	} 
}

function removeCourseBottom(str) {
    if (str == "") {
        return;
    } else {
		var x = document.getElementsByClassName("bottom");
		for(var i=0; i<x.length; i++)
		{
			if(x[i].innerHTML == str)
			{
				var parent = document.getElementById("choice");
				parent.removeChild(x[i]);
				break;
			}
		}
	}
}

function addCredit(cr){
	credit+=cr;
	if(credit>credit_limit)
	{
		credit-=cr;
		warning(2);
		return false;
	}
	else
	{ 
	document.getElementById("total_credit").innerHTML=credit;
	return true;
	}
}

function removeCredit(cr){
	credit-=parseFloat(cr);	
	document.getElementById("total_credit").innerHTML=credit;
	warning(0);
}

function warning(w){
	if(w==0)
	document.getElementById("warning").innerHTML="<br/>";	
	else if(w==1)
		document.getElementById("warning").innerHTML="Course already selected.";
	else if(w==2)
		document.getElementById("warning").innerHTML="Credit limit reached.";	
	
}

function insert_course_db(){
	var course_code= [];
	var x = document.getElementsByClassName("top");
	for(var i=0; i<x.length; i++)
	{
		//to do next: insert course code into variables, post to 'course_insert.php', edit 'course.php'	
	course_code[i]=x[i].title;
	}
	
	$.post('course_insert.php',{'total': total, 's1': course_code[0], 's2': course_code[1], 's3': course_code[2], 's4': course_code[3], 's5': course_code[4], 's6': course_code[5], 's7': course_code[6], 's8': course_code[7], 's9': course_code[8], 's10': course_code[9], 's11': course_code[10], 's12': course_code[11]}, 
	function (){
		alert("done");
		setTimeout(function(){ window.location = "student_home.php"; }, 1000);
		});
	
}

function total_fee()
{
	total= parseFloat(document.getElementById("total").title)+ (parseFloat(credit)*parseFloat(per_cr_fee));
	document.getElementById("full_total").innerHTML="সর্বমোটঃ "+total;
}

function insert_only_payment_db()
{
	total= parseFloat(document.getElementById("total").title);
	$.post('payment_request_academic.php',{'total': total}, 
	function (){
		alert("done");
		setTimeout(function(){ window.location = "student_home.php"; }, 1000);
		});
}