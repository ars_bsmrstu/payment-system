# University Fees Payment System

This repository includes a set of PHP, MySQL based dynamic web services designed for university students' fees payment system as well as all students' DBMS. This project was studied and developed keeping in mind of an actual scenario studied earlier in a particular field. All the necessary modules, dashboards are created for all users of the system including all the necessary functionalities.

Some important modules are highlighted below:
 
```
1. Central Admin Panel
2. Departmental Admin Panel
3. Hall Authority Admin Panel
4. Student's Personal Dashboard
5. Dashboard for Bank Officials
```

No need to fill up long forms if successfully deployed!

## Getting Started

Clone this repository to your workspace. Follow the Prerequisites, Database Info and build your own version! All codes are neat and self-explanatory.

### Prerequisites

You have the following development tools installed.

```
PHP 7 or later (check compatibility issues by yourself)
MySQL 5.6
```

### Database Info

The file "information.php" contains the database detailse. Modify to yours.

There are two database required.
This one is mandatory:
```
bsmrstue_payment_system.sql
```

Optional database(used for student first time registration, use appropriate source for your project):
```
bsmrstue_student_database.sql
```

Both of the database SQL is included in the root of the repository.

## Authors

* **Ratul Sikder**
* **Arun Biswas**
* **Shaon Karmaker**

See also the list of [contributors](https://bitbucket.org/ars_bsmrstu/payment-system/commits/all) who participated in this project.

## Acknowledgments

* This project was built as a 3rd year project of the contributing students of BSMRSTU, BD. Special thanks to "Agrani Bank, BSMRSTU Branch", "CSE Departmental Office, BSMRSTU", "Academic Section, BSMRSTU", "Saleh Ahmed, Assistant Professor and Former Chairman, CSE, BSMRSTU" for their support and encouragement.