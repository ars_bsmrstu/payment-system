-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 10, 2016 at 02:54 PM
-- Server version: 5.6.20
-- PHP Version: 5.5.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `bsmrstue_payment_system`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
  `account_no` varchar(25) NOT NULL,
  `time_start` date NOT NULL,
  `time_end` date NOT NULL,
  `course_registration` tinyint(1) NOT NULL,
  `fees` float NOT NULL,
  `admission_fee` float NOT NULL,
  `registration_fee` float NOT NULL,
  `transport` float NOT NULL,
  `library` float NOT NULL,
  `central_sports` float NOT NULL,
  `central_commonroom` float NOT NULL,
  `student_songsod` float NOT NULL,
  `student_welfare` float NOT NULL,
  `id_card` float NOT NULL,
  `medical_fee` float NOT NULL,
  `cultural` float NOT NULL,
  `scout` float NOT NULL,
  `bncc` float NOT NULL,
  `syllabus` float NOT NULL,
  `diary` float NOT NULL,
  `computer` float NOT NULL,
  `departmental_fee` float NOT NULL,
  `semester_exam_fee` float NOT NULL,
  `practical_exam_fee` float NOT NULL,
  `center_fee` float NOT NULL,
  `transcript_fee` float NOT NULL,
  `main_certificate` float NOT NULL,
  `provitional_certificate` float NOT NULL,
  `jamanot` float NOT NULL,
  `verification_fee` float NOT NULL,
  `admit_card` float NOT NULL,
  `baton_boi_fee` float NOT NULL,
  `jorimana` float NOT NULL,
  `publication` float NOT NULL,
  `others` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`account_no`, `time_start`, `time_end`, `course_registration`, `fees`, `admission_fee`, `registration_fee`, `transport`, `library`, `central_sports`, `central_commonroom`, `student_songsod`, `student_welfare`, `id_card`, `medical_fee`, `cultural`, `scout`, `bncc`, `syllabus`, `diary`, `computer`, `departmental_fee`, `semester_exam_fee`, `practical_exam_fee`, `center_fee`, `transcript_fee`, `main_certificate`, `provitional_certificate`, `jamanot`, `verification_fee`, `admit_card`, `baton_boi_fee`, `jorimana`, `publication`, `others`) VALUES
('122333464644', '2016-02-05', '2016-02-19', 1, 200, 100, 100, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `admin_login`
--

CREATE TABLE IF NOT EXISTS `admin_login` (
  `user_id` varchar(16) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin_login`
--

INSERT INTO `admin_login` (`user_id`, `password`) VALUES
('admin', '$2y$10$TrY7ETKoKRnzzCdc6elC.erdofaUYjURN.oJH6gqAOlWTyAFbNE7u');

-- --------------------------------------------------------

--
-- Table structure for table `course`
--

CREATE TABLE IF NOT EXISTS `course` (
  `course_code` varchar(11) NOT NULL,
  `course_title` varchar(30) NOT NULL,
  `credits` int(11) NOT NULL,
  `contact_hours` int(11) NOT NULL,
  `department` varchar(20) NOT NULL,
  `year` int(11) NOT NULL,
  `semester` int(11) NOT NULL,
  `session` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `course`
--

INSERT INTO `course` (`course_code`, `course_title`, `credits`, `contact_hours`, `department`, `year`, `semester`, `session`) VALUES
('EEE 202', 'Electromagnetics', 2, 22, '3', 2, 1, '5'),
('EEE123', 'EEE Basic', 2, 26, '1', 1, 1, '5'),
('ENG104 ', 'Technical English', 2, 26, '1', 1, 1, '5'),
('ENG105', 'English Language Lab I', 1, 26, '1', 1, 1, '5');

-- --------------------------------------------------------

--
-- Table structure for table `course_choice`
--

CREATE TABLE IF NOT EXISTS `course_choice` (
  `student_id` varchar(12) NOT NULL,
  `paid` tinyint(1) NOT NULL DEFAULT '0',
  `department_verification` tinyint(1) NOT NULL DEFAULT '0',
  `exam_controller_verification` tinyint(1) NOT NULL DEFAULT '0',
  `course_1` varchar(10) DEFAULT NULL,
  `course_2` varchar(10) DEFAULT NULL,
  `course_3` varchar(10) DEFAULT NULL,
  `course_4` varchar(10) DEFAULT NULL,
  `course_5` varchar(10) DEFAULT NULL,
  `course_6` varchar(10) DEFAULT NULL,
  `course_7` varchar(10) DEFAULT NULL,
  `course_8` varchar(10) DEFAULT NULL,
  `course_9` varchar(10) DEFAULT NULL,
  `course_10` varchar(10) DEFAULT NULL,
  `course_11` varchar(10) DEFAULT NULL,
  `course_12` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `course_choice`
--

INSERT INTO `course_choice` (`student_id`, `paid`, `department_verification`, `exam_controller_verification`, `course_1`, `course_2`, `course_3`, `course_4`, `course_5`, `course_6`, `course_7`, `course_8`, `course_9`, `course_10`, `course_11`, `course_12`) VALUES
('20121201047', 1, 1, 0, 'EEE123', 'ENG105', 'ENG104 ', '', '', '', '', '', '', '', '', ''),
('20121201051', 1, 1, 0, 'EEE123', 'ENG104 ', '', '', '', '', '', '', '', '', '', ''),
('20151201066', 1, 1, 0, 'EEE 202', '', '', '', '', '', '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `departments`
--

CREATE TABLE IF NOT EXISTS `departments` (
  `department_code` int(11) NOT NULL,
  `department_name` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `departments`
--

INSERT INTO `departments` (`department_code`, `department_name`) VALUES
(1, 'Computer Science and Engineering'),
(2, 'Applied Physics, Electronics and Communication Engineering'),
(3, 'Electrical and Electronic Engineering'),
(4, 'Mathematics'),
(5, 'Statistics'),
(6, 'Analytical and Environmental Chemistry'),
(7, 'Pharmacy'),
(8, 'Biotechnology and Genetic Engineering'),
(9, 'Biochemistry and Molecular Biology'),
(10, 'Agriculture'),
(11, 'English'),
(12, 'Bangla'),
(13, 'Economics'),
(14, 'Sociology'),
(15, 'Public Administration'),
(16, 'International Relation'),
(17, 'Management Studies'),
(18, 'Accounting and Information Systems'),
(19, 'Marketing'),
(20, 'Law');

-- --------------------------------------------------------

--
-- Table structure for table `department_login`
--

CREATE TABLE IF NOT EXISTS `department_login` (
  `user_id` varchar(16) NOT NULL,
  `department` int(11) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `department_login`
--

INSERT INTO `department_login` (`user_id`, `department`, `password`) VALUES
('cse_office', 1, '$2y$10$TrY7ETKoKRnzzCdc6elC.erdofaUYjURN.oJH6gqAOlWTyAFbNE7u'),
('eee_office', 3, '$2y$10$TrY7ETKoKRnzzCdc6elC.erdofaUYjURN.oJH6gqAOlWTyAFbNE7u');

-- --------------------------------------------------------

--
-- Table structure for table `error`
--

CREATE TABLE IF NOT EXISTS `error` (
  `error_number` int(11) NOT NULL,
  `sql_error` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `error`
--

INSERT INTO `error` (`error_number`, `sql_error`) VALUES
(0, 'Unknown column ''hisab'' in ''field list'''),
(0, 'Unknown column ''hisab'' in ''field list'''),
(0, 'Unknown column ''hisab'' in ''field list'''),
(0, 'Unknown column ''hisab'' in ''field list'''),
(0, 'Unknown column ''sonjuti'' in ''field list''');

-- --------------------------------------------------------

--
-- Table structure for table `hall`
--

CREATE TABLE IF NOT EXISTS `hall` (
  `hall_id` int(11) NOT NULL,
  `hall_name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hall`
--

INSERT INTO `hall` (`hall_id`, `hall_name`) VALUES
(1, 'Bangamata Sheikh Fazilatunnesa Mujib Hall'),
(11, 'Bijoy Debash Hall'),
(12, 'Swadhinata Debash Hall');

-- --------------------------------------------------------

--
-- Table structure for table `hall_account`
--

CREATE TABLE IF NOT EXISTS `hall_account` (
  `hisab` varchar(20) NOT NULL,
  `sonjuti` int(11) NOT NULL,
  `sitvara` int(11) NOT NULL,
  `songsthapn` int(11) NOT NULL,
  `jamant` int(11) NOT NULL,
  `k` int(11) NOT NULL,
  `kh` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hall_account`
--

INSERT INTO `hall_account` (`hisab`, `sonjuti`, `sitvara`, `songsthapn`, `jamant`, `k`, `kh`) VALUES
('123456', 0, 150, 200, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `hall_admin`
--

CREATE TABLE IF NOT EXISTS `hall_admin` (
  `username` varchar(30) NOT NULL,
  `password` varchar(100) NOT NULL,
  `hall` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hall_admin`
--

INSERT INTO `hall_admin` (`username`, `password`, `hall`) VALUES
('sdhall', '$1$bz3.IS4.$9fZW55SwC81GHVmbrDoZr.', '12'),
('bdhall', '$1$3q4.OT3.$8rMj1wcfid88LcieKn3h.1', '11');

-- --------------------------------------------------------

--
-- Table structure for table `hall_student_information`
--

CREATE TABLE IF NOT EXISTS `hall_student_information` (
  `admission_roll_no` varchar(12) NOT NULL,
  `room_number` int(11) NOT NULL,
  `broder_id` varchar(11) NOT NULL,
  `month` int(19) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hall_student_information`
--

INSERT INTO `hall_student_information` (`admission_roll_no`, `room_number`, `broder_id`, `month`) VALUES
('', 0, '', 0),
('20121201047', 206, 'A', 0),
('20121201051', 210, 'A', 3);

-- --------------------------------------------------------

--
-- Table structure for table `payment`
--

CREATE TABLE IF NOT EXISTS `payment` (
`serial` bigint(20) unsigned NOT NULL,
  `id` varchar(12) NOT NULL,
  `type` varchar(16) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `amount` int(11) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `payment`
--

INSERT INTO `payment` (`serial`, `id`, `type`, `date`, `amount`) VALUES
(2, '20121201051', 'academic', '2016-01-30 20:33:40', 350),
(3, '20121201047', 'academic', '2016-01-30 20:33:50', 280),
(4, '20151201066', 'academic', '2016-01-31 19:24:27', 366),
(5, '20151201066', 'academic', '2016-01-31 20:18:55', 266),
(6, '20121201051', 'academic', '2016-02-05 20:24:18', 800);

-- --------------------------------------------------------

--
-- Table structure for table `payment_request`
--

CREATE TABLE IF NOT EXISTS `payment_request` (
`serial` bigint(20) NOT NULL,
  `id` varchar(12) NOT NULL,
  `type` varchar(20) NOT NULL,
  `fee` float NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `payment_request`
--

INSERT INTO `payment_request` (`serial`, `id`, `type`, `fee`) VALUES
(2, '20121201047', 'academic', 280),
(6, '20151201066', 'academic', 266),
(7, '20121201051', 'academic', 800),
(8, '20121201051', 'hall', 700);

-- --------------------------------------------------------

--
-- Table structure for table `semister`
--

CREATE TABLE IF NOT EXISTS `semister` (
`semister_code` int(11) NOT NULL,
  `semister_name` varchar(20) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `semister`
--

INSERT INTO `semister` (`semister_code`, `semister_name`) VALUES
(1, 'First Semister'),
(2, 'Second Semister');

-- --------------------------------------------------------

--
-- Table structure for table `session`
--

CREATE TABLE IF NOT EXISTS `session` (
`session_code` int(11) NOT NULL,
  `session_name` varchar(15) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `session`
--

INSERT INTO `session` (`session_code`, `session_name`) VALUES
(1, '2011-2012'),
(2, '2012-2013'),
(3, '2013-2014'),
(4, '2014-2015'),
(5, '2015-16'),
(6, '2016-2017'),
(7, '2017-2018');

-- --------------------------------------------------------

--
-- Table structure for table `student_login`
--

CREATE TABLE IF NOT EXISTS `student_login` (
  `user_id` varchar(32) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `student_login`
--

INSERT INTO `student_login` (`user_id`, `password`) VALUES
('20121201047', '$2y$10$HxpYPcujFn8cuH6b85Z6z.6nGj8gdWPQybfDL9gpTwmMT17p5v1ka'),
('20121201051', '$2y$10$VNzFmCkpm4y5Xp8P/o.Mp.WjSU6tcZ24jqyQjyrD6zaaeBq0IGTwS'),
('20151201066', '$2y$10$szWvYc4Oe0pfIlgWe2hc6ubdPPb6.2sA7MnXSxGn8o3Z33TALDopy');

-- --------------------------------------------------------

--
-- Table structure for table `year`
--

CREATE TABLE IF NOT EXISTS `year` (
`year_code` int(11) NOT NULL,
  `year_name` varchar(15) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `year`
--

INSERT INTO `year` (`year_code`, `year_name`) VALUES
(1, 'First Year'),
(2, 'Second Year'),
(3, 'Thired Year'),
(4, 'Fourth Year');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin_login`
--
ALTER TABLE `admin_login`
 ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `course`
--
ALTER TABLE `course`
 ADD PRIMARY KEY (`course_code`);

--
-- Indexes for table `course_choice`
--
ALTER TABLE `course_choice`
 ADD PRIMARY KEY (`student_id`);

--
-- Indexes for table `departments`
--
ALTER TABLE `departments`
 ADD PRIMARY KEY (`department_code`);

--
-- Indexes for table `department_login`
--
ALTER TABLE `department_login`
 ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `hall`
--
ALTER TABLE `hall`
 ADD PRIMARY KEY (`hall_id`);

--
-- Indexes for table `hall_student_information`
--
ALTER TABLE `hall_student_information`
 ADD PRIMARY KEY (`admission_roll_no`);

--
-- Indexes for table `payment`
--
ALTER TABLE `payment`
 ADD PRIMARY KEY (`serial`);

--
-- Indexes for table `payment_request`
--
ALTER TABLE `payment_request`
 ADD PRIMARY KEY (`serial`);

--
-- Indexes for table `semister`
--
ALTER TABLE `semister`
 ADD PRIMARY KEY (`semister_code`);

--
-- Indexes for table `session`
--
ALTER TABLE `session`
 ADD PRIMARY KEY (`session_code`);

--
-- Indexes for table `student_login`
--
ALTER TABLE `student_login`
 ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `year`
--
ALTER TABLE `year`
 ADD PRIMARY KEY (`year_code`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `payment`
--
ALTER TABLE `payment`
MODIFY `serial` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `payment_request`
--
ALTER TABLE `payment_request`
MODIFY `serial` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `semister`
--
ALTER TABLE `semister`
MODIFY `semister_code` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `session`
--
ALTER TABLE `session`
MODIFY `session_code` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `year`
--
ALTER TABLE `year`
MODIFY `year_code` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
