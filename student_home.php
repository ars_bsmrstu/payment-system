<?php
// Start the session
session_start();

if(!$_SESSION["isLogin"])
{
	header("Location: index.php");
	die();
}

error_reporting(0);

include 'information.php';

mysql_connect("$db_host","$db_username","$db_password");

        mysql_select_db("$student_database");
		
		$admission_roll_no=$_SESSION['user_id'];

        $sql = "SELECT * FROM `student_personal_information` WHERE admission_roll='$admission_roll_no';";
        $result = mysql_query ($sql) or die (mysql_error ());
       while ($row = mysql_fetch_array($result)){     // here too, you put a space between it
             
			$admission_roll_no=$row['admission_roll'];
			$admission_session=$row['admission_session'];  
			$student_name=$row['student_name'];
			$stu_name_bng=$row['student_name_bng'];
			$father_name=$row['father_name'];
            $father_name_bng=$row['father_name_bng'];
			$mothers_name=$row['mother_name'];
            $mother_name_bng=$row['mother_name_bng'];
			$nationality=$row['nationality'];
			$religion_id=$row['religion'];
			$admission_session=$row['admission_session'];
			$d=$row['department'];
			$date_of_birth=$row['date_of_birth'];
			$phone_number=$row['phone_no'];
			$email=$row['email_address'];
			$blood=$row['blood_group'];
			$ssc_passing_year=$row['ssc_passing_year'];
            $ssc_institute=$row['ssc_institute'];
            $ssc_board_id=$row['ssc_board'];
            $ssc_roll=$row['ssc_roll'];
            $ssc_gpa=$row['ssc_grade'];
            $hsc_passing_year=$row['hsc_passing_year'];
            $hsc_institute=$row['hsc_institute'];
            $hsc_board_id=$row['hsc_board'];
            $hsc_roll=$row['hsc_roll'];
            $hsc_gpa=$row['hsc_grade'];
			$present_address=$row['present_address'];
            $village=$row['per_village'];
            $post=$row['per_post'];
            $upazilla=$row['per_upazilla'];
            $zilla_id=$row['per_zilla'];
			
	 }
	 
	 
	 // this is code to display
	  mysql_connect("$db_host","$db_username","$db_password")or die("Cannot connect to database"); //keep your db name
         mysql_select_db("$student_database") or die("Cannot select database");
       
         $sql = "SELECT * FROM `test_image` where `admission_roll_no`='".$_SESSION['user_id']."'"; // manipulate id ok 
         $sth = mysql_query($sql);
         $result=mysql_fetch_array($sth);
?>

<!doctype html>
<html lang="en-US">
<head>
<meta charset="utf-8">
<meta http-equiv="Content-Type" content="text/html">
<title>Student Home</title>
<meta name="author" content="Jake Rocheleau">
<link rel="shortcut icon" href="Bsmrstu_logo.jpg">
<link rel="icon" href="Bsmrstu_logo.jpg">
<link rel="stylesheet" type="text/css" media="all" href="css/styles_student_home.css">
<script type="text/javascript" src="js/jquery-1.11.1.min.js"></script>

<script>    
		function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#blah')
                    .attr('src', e.target.result)
                    .width(200)
                    .height(200);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }

</script>

</head>

<body>
<!-- header id="hero"></header> -->
<!-- http://unsplash.com/post/96058742904/download-by-jonas-nilsson-lee -->
<img src="bsmrstu.jpg" width="100%"/>
<div id="content">
  <div class="wrapper">
    <h1>Welcome <?php echo $student_name; ?> </h1>
    <div id="jobs" class="clearfix">
      <div id="jobs-list">
        <ul>
          
          <li><?php echo '<img src="data:image/jpeg;base64,'.base64_encode( $result['image'] ).'"/>' ?></li>
          <li><a href="#job1" class="active">Your Information</a></li>
          <li><a href="#job2">Edit Your Information</a></li>
          <li><a href="#job3">Change Your Image</a></li>
          <li><a href="#job4">Exam Payment/Hall Payment</a></li>
          <li><a href="#job5">Logout</a></li>
          <li hidden="hidden"><a href="#job6" id="subject_choice">test</a></li>
          
        </ul>
      </div>
      <div id="job-info">
        <div id="job1" class="jobitem displayed">
          <div class="info_image">
            <div class="info">
              <form class="form-style-4" action="" method="post">
			  <p style="color:white"><strong>Student Information</strong></p>
                <label for="admission_roll_no"> <span>ID No :</span>
                  <input name="admission_roll_no"  type="text" value="<?php echo $admission_roll_no; ?>"  required="true"  id="admission_roll_no" readonly  maxlength="11" />
                </label>
                <label for="admission_session"> <span>Admission Session :</span>
                  <input name="admission_session" type="text" required="true" class="session"  id="admission_session" pattern="20[0-9]{2}\-[0-9]{2}" onFocus="instruction(admission_session)" value="<?php echo $admission_session; ?>" maxlength="10" readonly style="color:#9A9A9A" />
                </label>
                <label for="department"> <span>Departemnts :</span>
                  <input name="department" onFocus="instruction(admission_roll_no)" readonly type="text" value="<?php 
		$sql = "SELECT * FROM `departments` WHERE department_code='$d';";
            $result = mysql_query ($sql) or die (mysql_error ()); 
			while ($row = mysql_fetch_array($result)){
				
				$department=$row['department_name'];

			}
		echo $department; ?>" required="true" id="department"  />
                </label>
                <label>Student Name: </label>
                <label for="student_name"><span>In English:</span>
                  <input  name="student_name" value="<?php echo $student_name; ?>" type="text"  required="true" class="session" id="student_Name" value readonly onFocus="instruction(student_name)" />
                </label>
                <label for="student_name_bng"><span>বাংলায় :</span>
                  <input  name="student_name_bng"  readonly type="text" value="<?php echo $stu_name_bng;; ?>" required="true" class="session" id="student_Name" onFocus="instruction(student_name)" />
                </label>
                <label>Father's Name: </label>
                <label for="fathers_name"> <span>In English:</span>
                  <input style="color:#9A9A9A" name="fathers_name" readonly value="<?php echo $father_name; ?>" readonly type="text" required class="form_table" id="fathers_name" onFocus="instruction(fathers_name)" >
                </label>
                <label for="father_name_bng"><span>বাংলায় :</span>
                  <input  name="father_name_bng"  type="text" readonly value="<?php echo $father_name_bng;?>" required="true" class="session" id="father_Name" onFocus="instruction(student_name)" />
                  <br>
                </label>
                <label>Mother's Name : </label>
                <label for="mothers_name"> <span>In English :</span>
                  <input style="color:#9A9A9A" name="mothers_name" value="<?php echo $mothers_name; ?>" type="text" required id="mothers_name" value readonly onFocus="instruction(mothers_name)" />
                </label>
                <label for="mother_name_bng"><span>বাংলায় :</span>
                  <input  name="mother_name_bng"  type="text"  value="<?php echo $mother_name_bng; ?>" required="true" class="session" id="mother_Name" readonly onFocus="instruction(student_name)" />
                </label>
                <br>
                <label for="nationality"> <span>Nationility :</span>
                  <input name="nationality" onFocus="instruction(nationality)" value="<?php echo $nationality; ?>" type="text" required="true" class="session" value="Bangladeshi" readonly required id="nationality" pattern="^[A-Za-z\s]{1,}" maxlength="20" />
                </label>
                <label for="religion"> <span>Religion :</span>
                  <input style="color:#9A9A9A" name="religion" type="text" value="<?php 
		$sql = "SELECT * FROM `religion` WHERE religion_id='$religion_id';";
            $result = mysql_query ($sql) or die (mysql_error ()); 
			while ($row = mysql_fetch_array($result)){
				
				$religion=$row['religion_name'];

			}
		echo $religion; ?>" required="true" class="session" readonly required id="religion" value readonly onFocus="instruction(religion)" />
                </label>
                <label for="date_of_birth"> <span>Date of Birth :</span>
                  <input style="color:#9A9A9A" name="date_of_birth" type="text" value="<?php echo $date_of_birth; ?>" required="true" class="session" readonly id="date_of_birth" value readonly onFocus="instruction(date_of_birth)" />
                </label>
                <label for="phone_number"> <span>Phone Number :</span>
                  <input style="color:#9A9A9A" name="phone_number" type="text" value="<?php echo $phone_number; ?>" required="true" class="session" readonly  id="phone_number" value readonly onFocus="instruction(phone_number)" />
                </label>
                <label for="email"> <span>Email Address :</span>
                  <input style="color:#9A9A9A" name="email" type="text" value="<?php echo $email; ?>" required="true" class="session" id="email" readonly value readonly onFocus="instruction(email)" />
                </label>
                <label for="blood_group"> <span>Blood Group :</span>
                  <input  name="blood_group" readonly type="text" value="<?php $sql = "SELECT * FROM `blood` WHERE blood_id='$blood';";
            $result = mysql_query ($sql) or die (mysql_error ()); 
			while ($row = mysql_fetch_array($result)){
				
				$bld=$row['blood_group'];

			}
		echo $bld; ?>" required="true"  id="blood_group" maxlength="30"  />
                </label>
                <label for="present_address"> <span>Present Address :</span>
                  <input name="present_address" readonly onFocus="instruction(present_address)" type="text" value="<?php echo $present_address; ?>" required="true" id="present_address" style="width:50%" maxlength="140" />
                </label>
                <h4>Permanent Address :</h4>
                <label for="village"> <span> Village :</span>
                  <input name="village" onFocus="instruction()" readonly type="text" value="<?php echo $village; ?>" required="true" id="village" maxlength="30" />
                </label>
                <label for="post"> <span> Post :</span>
                  <input name="post" onFocus="instruction()" type="text" readonly value="<?php echo $post; ?>" required="true" id="post" maxlength="30" />
                </label>
                <label for="upazilla"> <span>Upazilla :</span>
                  <input name="upazilla" onFocus="instruction()" type="text" readonly value="<?php echo $upazilla; ?>" required="true" id="upazilla" maxlength="30" />
                </label>
                <label for="zilla"> <span>Zilla :</span>
                  <input name="zilla" onFocus="instruction()" type="text" readonly value="<?php $sql = "SELECT * FROM `district` WHERE zilla_code='$zilla_id';";
            $result = mysql_query ($sql) or die (mysql_error ()); 
			while ($row = mysql_fetch_array($result)){
				
				$zilla=$row['zilla_name'];

			}
		echo $zilla; ?>" required="true" id="zilla" maxlength="30" />
                </label>
              </form>
            </div>
            <!-- @end #info -->
            
            <div class="image"> </div>
            <!-- @end #imag --> 
            
          </div>
          <!-- @end #info_image -->
          
          <div class="table">
            <table style="width:100%">
              <caption>
              <label id="instruction_table" style="color:#FF0004;"></label>
              <strong>Previous Exam information:</strong>
              </caption>
              <tr>
                <th style="width:15%" >ExamName</th>
                <th style="width:11%" >Passing Year</th>
                <th style="width:30%">Institution Name </th>
                <th style="width:16%" >Bord/University</th>
                <th style="width:14%">Exam Roll Number</th>
                <th style="width:8%" >Division/GPA/CGPA</th>
              </tr>
              <tr>
                <th scope="row">SSC/Equivalent</th>
                <td ><?php echo $ssc_passing_year; ?></td>
                <td ><?php echo $ssc_institute; ?></td>
                <td ><?php $sql = "SELECT * FROM `education_board` WHERE education_board_id='$ssc_board_id';";
            $result = mysql_query ($sql) or die (mysql_error ()); 
			while ($row = mysql_fetch_array($result)){
				
				$bord_name=$row['education_board_name'];

			}
		echo $bord_name; ?></td>
                <td ><?php echo $ssc_roll; ?></td>
                <td ><?php echo$ssc_gpa; ?></td>
                </tr>
              <tr>
                <th scope="row">HSC/Equivalent</th>
                <td ><?php echo $hsc_passing_year; ?></td>
                <td ><?php echo $hsc_institute; ?></td>
                <td ><?php $sql = "SELECT * FROM `education_board` WHERE education_board_id='$hsc_board_id';";
            $result = mysql_query ($sql) or die (mysql_error ()); 
			while ($row = mysql_fetch_array($result)){
				
				$bord_name=$row['education_board_name'];

			}
		echo $bord_name; ?></td>
                <td ><?php echo $hsc_roll; ?></td>
                <td ><?php echo $hsc_gpa; ?></td>
                 </tr>
            </table>
          </div>
          <!-- @end #table --> 
          
        </div>
        <!-- @end #job1 -->
        
        <div id="job2" class="jobitem">
          
          
        <form class="form-style-4" action="student_edit.php" method="post">
        
         <p style="color:white"><strong>Edit Your Information Information</strong></p>
                
                
                <label for="phone_number"> <span>Phone Number :</span>
            <input name="phone_number" onFocus="instruction(phone_number)" type="text" value="<?php echo $phone_number; ?>"  required id="phone_number" pattern="\+{0,1}[0-9]{4,18}" maxlength="16" />
          </label>
          
          <label for="email"> <span>Email Address :</span>
            <input name="email" onFocus="instruction(email)" type="email" value="<?php echo $email; ?>"  required id="email" maxlength="40"/>
          </label>
          
          <label for="blood_group"> <span>Blood Group :</span>
            <select name="blood_group" value="<?php $sql = "SELECT * FROM `blood` WHERE blood_id='$blood';";
            $result = mysql_query ($sql) or die (mysql_error ()); 
			while ($row = mysql_fetch_array($result)){
				
				$bld=$row['blood_group'];

			}
		echo $bld; ?>" id="blood_group" onFocus="instruction(blood_group)">
             <option value="<?php $sql = "SELECT * FROM `blood` WHERE blood_id='$blood';";
            $result = mysql_query ($sql) or die (mysql_error ()); 
			while ($row = mysql_fetch_array($result)){
				
				$bld=$row['blood_group'];

			}
		echo $bld; ?>"><?php $sql = "SELECT * FROM `blood` WHERE blood_id='$blood';";
            $result = mysql_query ($sql) or die (mysql_error ()); 
			while ($row = mysql_fetch_array($result)){
				
				$bld=$row['blood_group'];

			}
		echo $bld; ?></option>
              <option value="unknown">Unknown</option>
              <option value="A+">A+</option>
              <option value="A-">A-</option>
              <option value="B+">B+</option>
              <option value="B-">B-</option>
              <option value="O+">O+</option>
              <option value="O-">O-</option>
              <option value="AB+">AB+</option>
              <option value="AB-">AB-</option>
            </select>
          </label>
          
          <label for="present_address"> <span>Present Address :</span>
          <input name="present_address" onFocus="instruction(present_address)" type="text" value="<?php echo $present_address; ?>" required="true" id="present_address" style="width:50%" maxlength="140" />
        </label>
        <h4>Permanent Address :</h4>
        <label for="village"> <span> Village :</span>
          <input name="village" onFocus="instruction()" type="text" value="<?php echo $village; ?>"  required="true" id="village" maxlength="30" />
        </label>
        <label for="post"> <span> Post :</span>
          <input name="post" onFocus="instruction()" type="text" value="<?php echo $post; ?>" required="true" id="post" maxlength="30" />
        </label>
        <label for="upazilla"> <span>Upazilla :</span>
          <input name="upazilla" onFocus="instruction()" type="text" value="<?php echo $upazilla; ?>" required="true" id="upazilla" maxlength="30" />
        </label>
        <label for="zilla"> <span>Zilla :</span>
          <select name="zilla"  id="zilla" onFocus="instruction()">
            <option value="<?php $sql = "SELECT * FROM `district` WHERE zilla_code='$zilla_id';";
            $result = mysql_query ($sql) or die (mysql_error ()); 
			while ($row = mysql_fetch_array($result)){
				
				$zilla=$row['zilla_name'];

			}
		echo $zilla; ?>"><?php $sql = "SELECT * FROM `district` WHERE zilla_code='$zilla_id';";
            $result = mysql_query ($sql) or die (mysql_error ()); 
			while ($row = mysql_fetch_array($result)){
				
				$zilla=$row['zilla_name'];

			}
		echo $zilla; ?></option>
            <option value="Bagerhat">Bagerhat</option>
            <option value="Bandarban">Bandarban</option>
            <option value="Barguna">Barguna</option>
            <option value="Barisal">Barisal</option>
            <option value="Bhola">Bhola</option>
            <option value="Bogra">Bogra</option>
            <option value="Brahmanbaria">Brahmanbaria</option>
            <option value="Chandpur">Chandpur</option>
            <option value="Chapainawabganj">Chapainawabganj</option>
            <option value="Chittagong">Chittagong</option>
            <option value="Chuadanga">Chuadanga</option>
            <option value="Comilla">Comilla</option>
            <option value="Coxs Bazar">Cox's Bazar</option>
            <option value="Dhaka">Dhaka</option>
            <option value="Dinajpur">Dinajpur</option>
            <option value="Faridpur">Faridpur</option>
            <option value="Feni">Feni</option>
            <option value="Gaibandha">Gaibandha</option>
            <option value="Gazipur">Gazipur</option>
            <option value="Gopalganj">Gopalganj</option>
            <option value="Habiganj">Habiganj</option>
            <option value="Jamalpur">Jamalpur</option>
            <option value="Jessore">Jessore</option>
            <option value="Jhalokati">Jhalokati</option>
            <option value="Jhenaidah">Jhenaidah</option>
            <option value="Joypurhat">Joypurhat</option>
            <option value="Khagrachhari">Khagrachhari</option>
            <option value="Khulna">Khulna</option>
            <option value="Kishoreganj">Kishoreganj</option>
            <option value="Kurigram">Kurigram</option>
            <option value="Kushtia">Kushtia</option>
            <option value="Lakshmipur">Lakshmipur</option>
            <option value="Lalmonirhat">Lalmonirhat</option>
            <option value="Madaripur">Madaripur</option>
            <option value="Magura">Magura</option>
            <option value="Manikganj">Manikganj</option>
            <option value="Meherpur">Meherpur</option>
            <option value="Moulvibazar">Moulvibazar</option>
            <option value="Munshiganj">Munshiganj</option>
            <option value="Mymensingh">Mymensingh</option>
            <option value="Naogaon">Naogaon</option>
            <option value="Narail">Narail</option>
            <option value="Narayanganj">Narayanganj</option>
            <option value="Narsingdi">Narsingdi</option>
            <option value="Natore">Natore</option>
            <option value="Netrakona">Netrakona</option>
            <option value="Nilphamari">Nilphamari</option>
            <option value="Noakhali">Noakhali</option>
            <option value="Pabna">Pabna</option>
            <option value="Panchagarh">Panchagarh</option>
            <option value="Patuakhali">Patuakhali</option>
            <option value="Pirojpur">Pirojpur</option>
            <option value="Rajbari">Rajbari</option>
            <option value="Rajshahi">Rajshahi</option>
            <option value="Rangamati">Rangamati</option>
            <option value="Rangpur">Rangpur</option>
            <option value="Satkhira">Satkhira</option>
            <option value="Shariatpur">Shariatpur</option>
            <option value="Sherpur">Sherpur</option>
            <option value="Sirajganj">Sirajganj</option>
            <option value="Sunamganj">Sunamganj</option>
            <option value="Sylhet">Sylhet</option>
            <option value="Tangail">Tangail</option>
            <option value="Thakurgaon">Thakurgaon</option>
          </select>
        </label>
                
        <div id="submit_buttom" >
        <label> <input type="submit" value="Submit"  class="myButton"> </label>
        </div>
        </form>
          
          
          </div><!-- @end #job2 -->
        
        <div id="job3" class="jobitem">
          
          
          <form class="form-style-4" action="change_image.php" method="post" enctype="multipart/form-data" accept-charset="utf-8">
        
         <p style="color:white"><strong>Change Your Image</strong></p>
                
        
    
     <div class="picture">
      <label for="uploadImage"> <span>Upload Image :</span>
      	<div class ="inf" ><input type='file' name="image"  required onchange="readURL(this);"/> </div>
    	<div class="image"><img id="blah" src="#" alt="" /></div>
        </label>
        </div>        
                
                
        <div id="submit_buttom" >
        <label> <input type="submit" name="submit" value="Submit"  class="myButton"> </label>
        </div>
        </form>
          
          
          
           </div><!-- @end #job3 -->
        
        <div id="job4" class="jobitem">
          <h3>Exam Payment/Hall Payment</h3>
          		<a href="subject_choice.php" class="applybtn"><span>Exam Payment</span></a>
          <a href="hall_payment.php" class="applybtn" ><span>Hall Payment</span></a>
           </div>
          
         
        <!-- @end #job4 -->
        
        <div id="job5" class="jobitem">
          <h3>Student Logout</h3>
          <a href="student_logout.php" class="applybtn"><span>Yes</span></a>
          <a href="student_home.php" class="applybtn"><span>No</span></a>
           </div>
        <!-- @end #job5 --> 
        <div id="job6" class="jobitem">
        <h1>hello</h1>
        </div>
        
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
$(function(){
  $jobslist = $('#jobs-list ul li a');
  
  $($jobslist).on('click', function(e){
    e.preventDefault();
    var newcontent = $(this).attr('href');

    if(!$(this).hasClass('active')) {
      $('#jobs-list ul li a.active').removeClass('active');
    }
    $(this).addClass('active');
    
    if(!$(newcontent).hasClass('displayed')) {
      $('.jobitem.displayed').removeClass('displayed');
      $(newcontent).addClass('displayed');
    }
  });
});
</script>
</body>
</html>